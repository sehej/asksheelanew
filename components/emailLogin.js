require('create-react-class');
import React from 'react';
import { StyleSheet, Platform, View, Text, Image,Alert, TouchableOpacity,TextInput, ImageBackground, ScrollView, StatusBar,  BackHandler } from 'react-native';
import {Icon, Button, Container, Header, Body, Content,Left, Right, Footer} from 'native-base';

export default class emailLogin extends React.Component{
    static navigationOptions={
        header:null
    }
    
    render(){
        return(
            <ScrollView>
                <View style={{flex:1, flexDirection:'column'}}>
                    <View style={{flex:1}}>
                        <Image source={require('../images/emaillogin/emailLogin.png')} style={{width:'100%'}}/>
                    </View>
                    <View style={{flex:1, marginTop:20, alignItems:'center'}}>
                        <TextInput
                        placeholder='Email Id'
                        placeholderTextColor='grey'
                        style={styles.textinput}/>
                        <TextInput
                        placeholder='Password'
                        placeholderTextColor='grey'
                        style={styles.textinput}/>
                        
                        <TouchableOpacity
                        style={styles.button}
                        activeOpacity={0.8}
                        >
                            <Text style={styles.buttonText}>Sign In</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                        activeOpacity={0.8}
                        >
                        <Text style={{marginTop:10}}>
                            Forgot Password?   
                        </Text>
                        </TouchableOpacity>
                        
                        <Text style={styles.foot}>
                            Didn't have account? <Text style={{color:'#EF5350', fontWeight:'bold'}}>SIGN UP</Text>    
                        </Text>
                        
                    </View>
                    
                </View> 
           </ScrollView>




        );
    }
}

const styles = StyleSheet.create({

    textinput:{
        width:'70%',
        paddingHorizontal:16,
        fontSize:16,
        marginTop:10,
    },
    button:{
        width:'65%',
        backgroundColor:'#EF5350',
        borderRadius:20,
        marginTop:30,
        alignItems:'center',
    },
    buttonText:{
        color:'white',
        fontSize:16,
        paddingTop:10,
        paddingBottom:10
    },
    foot:{
      marginTop:'29%'
    }

});